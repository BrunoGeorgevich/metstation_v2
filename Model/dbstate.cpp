#include "dbstate.h"

/*
 * [Construtor]
 *
 * Descrição: Executa o método construtor da
 * classe DBState.
 *
 * Parâmetros:
 *  - QVariantMap properties : Mapa com as propriedades do Estado
 *  - QObject *parent : Ponteiro para o QObject Pai *
 */
DBState::DBState(QVariantMap properties, QObject *parent) : QObject(parent)
{
    //Define as propriedades do estado do sistema
    setProperties(properties);
}

/*
 * [Método]
 *
 * Descrição: Imprime o estado e suas propriedades
 * na tela do console.
 *
 * Parâmetros:
 *  - void
 */
void DBState::printState()
{
    //Cria uma string com todos os dados do estado
    QString buffer = "";
    buffer += "TimeStamp : " + m_timeStamp.toString() + "\n";
    buffer += "Version : " + QString::number(m_version) + "\n";
    buffer += "Wind Direction : " + QString::number(m_windDirection) + "\n";
    buffer += "Wind Velocity : " + QString::number(m_windVelocity) + "\n";
    buffer += "Temperature : " + QString::number(m_temperature) + "\n";
    buffer += "Humidity : " + QString::number(m_humidity) + "\n";
    buffer += "Accumulate Rain : " + QString::number(m_accumulatedRain) + "\n";
    buffer += "ATM Pressure : " + QString::number(m_pressureATM) + "\n";
    buffer += "Luminosity : " + QString::number(m_luminosity) + "\n";
    //Imprime a string
    qDebug() << buffer;
}

/*
 * [Método]
 *
 * Descrição: Define as propriedades do Estado.
 *
 * Parâmetros:
 *  - QVariantMap properties : Mapa com as propriedades
 */
void DBState::setProperties(QVariantMap properties)
{
    //Coleta as propriedades e as define no estado
    m_timeStamp = properties.value("date").toDateTime();
    m_version = properties.value("id").toInt();
    m_windDirection = properties.value("wind_direction").toInt();
    m_windVelocity = properties.value("wind_velocity").toFloat();
    m_temperature = properties.value("temperature").toFloat();
    m_humidity = properties.value("humidity").toFloat();
    m_accumulatedRain = properties.value("accumulated_rain").toFloat();
    m_pressureATM = properties.value("pressure_atm").toFloat();
    m_luminosity = properties.value("luminosity").toFloat();
    m_error = false;
    m_errorString = "No Error!";
}

/*
 * [Método]
 *
 * Descrição: Retorna qualquer propriedade do Estado.
 *
 * Parâmetros:
 *  - QByteArray key : Nome da propriedade desejada
 */
float DBState::getProperty(QByteArray key)
{
    //Retorna o valor das propriedades
    if(key == "date")
        return (float) m_timeStamp.toSecsSinceEpoch();
    else if(key == "id")
        return m_version;
    else if(key == "wind_direction")
        return m_windDirection;
    else if(key == "wind_velocity")
        return m_windVelocity;
    else if(key == "temperature")
        return m_temperature;
    else if(key == "humidity")
        return m_humidity;
    else if(key == "accumulated_rain")
        return m_accumulatedRain;
    else if(key == "pressure_atm")
        return m_pressureATM;
    else if(key == "luminosity")
        return m_luminosity;
    else {
        qDebug() << "NON EXISTENT PROPERTY : " << key;
        return -1;
    }
}
