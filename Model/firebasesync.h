#ifndef FIREBASESYNC_H
#define FIREBASESYNC_H

#include <QProcess>
#include <QObject>
#include <QString>
#include <QFile>
#include <QFileInfo>

#include "dbstate.h"
#include "../Controller/methodgraphpoint.h"

#include "jsontranslator.h"

class FirebaseSync : public QObject
{
    Q_OBJECT
public:
    FirebaseSync(QObject *parent = 0);
    bool isFirebaseDatabaseEmpty();
    QString insertIntoFirebaseDatabase(QString node, QString jsonData);
    QVariantMap getFirebaseDatabase(QString node);
private slots:
    void hasNewMethodCalculated(MethodGraphPoint *point);
    void hasNewMeanDataCalculated(DBState *meanState);
private:
    void initPyFiles();
    QString getDatabaseFirebaseJSON(QString node, QString scriptPath);
    QString insertDatabaseFirebaseJSON(QString node, QString jsonData);
};

#endif // FIREBASESYNC_H
