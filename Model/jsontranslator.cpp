#include "jsontranslator.h"

/*
 * [Método]
 *
 * Descrição: Recebe uma string formatada como um JSON válido e
 * retorna um QJsonObject * da mesma.
 *
 * Parâmetros:
 *  - QByteArray data : String JSON válida
 */
QJsonObject *JsonTranslator::translate(QByteArray data)
{
    //Instancia o parse, caso exista algum erro na validação do JSON
    QJsonParseError *error = new QJsonParseError();
    //Instancia o objeto que portará os dados do JSON
    QJsonObject *object = new QJsonObject(QJsonDocument::fromJson(data, error).object());

    //Verifica se houve algum erro de validação no JSON
    if(error->error != QJsonParseError::NoError) {
        qDebug() << "JSON ERROR : " + error->errorString()/* + "\nCONTENT :" << data*/;
        return NULL;
    }

    return object;
}

/*
 * [Método]
 *
 * Descrição: Recebe um mapa de propriedades e retorna uma string
 * formatada como um JSON válido.
 *
 * Parâmetros:
 *  - QVariantMap properties : Mapa de propriedades
 */
QString JsonTranslator::translateToJson(QVariantMap properties)
{
    //Instancia o objeto que receberá as propriedades
    QJsonObject object;
    //Itera entra as propriedades, as atribuindo ao objeto
    foreach (QString key, properties.keys()) {
        QVariant value = properties.value(key);
        if(value.typeName() == "int")
            object.insert(key, value.toInt());
        else
            object.insert(key, value.toFloat());
    }

    //Retorna um parse do objeto para uma string JSON
    QJsonDocument doc(object);
    return doc.toJson();
}
