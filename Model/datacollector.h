#ifndef DATACOLLECTOR_H
#define DATACOLLECTOR_H

#include <QDateTime>
#include <QObject>

//Network Includes
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>

//Custom Includes
#include "jsontranslator.h"
#include "wificonnection.h"
#include "dbstate.h"

#include "../Controller/definitions.h"

class DataCollector : public QObject
{
    Q_OBJECT
public:
    explicit DataCollector(QObject *parent = 0);
    int getLastVersion() const;
    void setLastVersion(int value);
    void resetVersion();
signals:
    void verifyNewState(DBState*);
    void newDBState(DBState *state);
private slots:
    void portReplyArrived(QString data);
    void sendNewState(DBState *state);
private:
    int m_lastVersion;
    WifiConnection *m_localWifiConnection;
};

#endif // DATACOLLECTOR_H
