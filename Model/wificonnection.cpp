#include "wificonnection.h"

/*
 * [Construtor]
 *
 * Descrição: Executa o método construtor da
 * classe WifiConnection.
 *
 * Parâmetros:
 *  - QObject *parent : Ponteiro para o QObject Pai *
 */
WifiConnection::WifiConnection(QObject *parent) : QTcpServer(parent)
{
    //Conecta as novas conexões ao slot slotNewConnection
    QObject::connect(this,SIGNAL(newConnection()),SLOT(slotNewConnection()));
    //Define que o servidor TCP está ouvindo a porta 50939
    listen(QHostAddress::Any,50939);
    //Define que a conexão é nula
    connection = NULL;
}

/*
 * [Método/Slot]
 *
 * Descrição: Trata as novas conexões via WiFi da estação metereológica.
 *
 * Parâmetros:
 *  - void
 */
void WifiConnection::slotNewConnection(){
    if(connection != NULL)
        delete connection;
    connection = nextPendingConnection();
    connect(connection,SIGNAL(readyRead()),SLOT(readData()));
}

/*
 * [Método]
 *
 * Descrição: Lê os dados enviados pela estação metereológica e
 * envia um sinal com os mesmos.
 *
 * Parâmetros:
 *  - void
 */
void WifiConnection::readData(){
    emit this->readPort(connection->readAll());
}
