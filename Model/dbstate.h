#ifndef DBSTATE_H
#define DBSTATE_H

#include <QDate>
#include <QDebug>
#include <QObject>
#include <QVariantMap>

//Custom Libs
#include "../Essencials/qqmlobjectlistmodel.h"
#include "../Essencials/qqmlhelpers.h"

class DBState : public QObject
{
    Q_OBJECT

    QML_WRITABLE_PROPERTY(QDateTime, timeStamp)
    QML_WRITABLE_PROPERTY(int, version)
    QML_WRITABLE_PROPERTY(int, windDirection)
    QML_WRITABLE_PROPERTY(float, windVelocity)
    QML_WRITABLE_PROPERTY(float, temperature)
    QML_WRITABLE_PROPERTY(float, humidity)
    QML_WRITABLE_PROPERTY(float, accumulatedRain)
    QML_WRITABLE_PROPERTY(float, pressureATM)
    QML_WRITABLE_PROPERTY(float, luminosity)
//    QML_WRITABLE_PROPERTY(float, battery)
    QML_WRITABLE_PROPERTY(bool, error)
    QML_WRITABLE_PROPERTY(QString, errorString)
public:
    explicit DBState(QVariantMap properties, QObject *parent = 0);
    void printState();
    void setProperties(QVariantMap properties);
    float getProperty(QByteArray key);
};

#endif // DBSTATE_H
