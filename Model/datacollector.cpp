#include "datacollector.h"

/*
 * [Construtor]
 *
 * Descrição: Executa o método construtor da
 * classe DataCollector.
 *
 * Parâmetros:
 *  - QObject *parent : Ponteiro para o QObject Pai *
 */
DataCollector::DataCollector(QObject *parent) : QObject(parent)
{
    //Define o ID do último dado = 0
    m_lastVersion = 0;

    //Instancia a conexão wifi com o Arduino
    m_localWifiConnection = new WifiConnection(this);
    connect(m_localWifiConnection, SIGNAL(readPort(QString)),
            this, SLOT(portReplyArrived(QString)));

    //Conecta o sinal verifyNewState ao slot sendNewState
    connect(this, SIGNAL(verifyNewState(DBState*)),
            this, SLOT(sendNewState(DBState*)));
}

/*
 * [Método/Slot]
 *
 * Descrição: Recebe os dados enviados pelo arduino e
 * faz o tratamento deles.
 *
 * Parâmetros:
 *  - QString data : String que contém os dados enviados
 */
void DataCollector::portReplyArrived(QString data)
{
    //Particiona a string dos dados no caractér ','
    QStringList dataList = data.split(",");
    //Adiciona os dados a um mapa de propriedades
    QVariantMap properties;
    properties.insert("date",QDateTime::currentDateTime());
    properties.insert("id", m_lastVersion);
    properties.insert("wind_direction", dataList[WINDDIRECTION].toInt());
    properties.insert("wind_velocity", dataList[WINDVELOCITY].toFloat());
    properties.insert("temperature", dataList[TEMPERATURE].toFloat());
    properties.insert("humidity", dataList[HUMIDITY].toFloat());
    properties.insert("accumulated_rain", dataList[ACCUMULATEDRAIN].toFloat());
    properties.insert("pressure_atm", dataList[PRESSUREATM].toFloat());
    properties.insert("luminosity", dataList[LUMINOSITY].toFloat());

    //Cria um novo estado
    DBState *state = new DBState(properties);

    //Envia o novo estado por um sinal
    emit verifyNewState(state);
}

/*
 * [Método/Slot]
 *
 * Descrição: Incrementa o id do próximo dado e envia
 * o novo estado para os bancos de dados.
 *
 * Parâmetros:
 *  - DBState *state : Novo estado
 */
void DataCollector::sendNewState(DBState *state)
{
    //Incrementa o id do próximo dado
    m_lastVersion += 1;
    //Envia o novo estado para os bancos de dados.
    emit newDBState(state);
}

/*
 * [Método]
 *
 * Descrição: Retorna o id do próximo dado a ser adicionado.
 *
 * Parâmetros:
 *  - void
 */
int DataCollector::getLastVersion() const
{
    return m_lastVersion;
}

/*
 * [Método]
 *
 * Descrição: Define o id do próximo dado a ser adicionado.
 *
 * Parâmetros:
 *  - int value : valor do próximo ID
 */
void DataCollector::setLastVersion(int value)
{
    m_lastVersion = value;
}

/*
 * [Método]
 *
 * Descrição: Define o id do próximo dado a ser adicionado
 * para 0.
 *
 * Parâmetros:
 *  - void
 */
void DataCollector::resetVersion()
{
    setLastVersion(0);
}
