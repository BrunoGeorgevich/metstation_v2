#include "firebasesync.h"


/*
 * [Defines]
 *
 * Descrição: Define os caminhos padrões do arquivos
 * scripts do Python.
 *
 * Parâmetros:
 *  - void
 */
#define GET_FIREBASE_PATH ":/python/getFromFirebase.py"
#define GET_FIREBASE_NAME "getFromFirebase.py"
#define PUSH_FIREBASE_PATH ":/python/insertIntoFirebase.py"
#define PUSH_FIREBASE_NAME "insertIntoFirebase.py"


/*
 * [Construtor]
 *
 * Descrição: Executa o método construtor da
 * classe FirebaseSync.
 *
 * Parâmetros:
 *  - QObject *parent : Ponteiro para o QObject Pai *
 */
FirebaseSync::FirebaseSync(QObject *parent) : QObject(parent)
{
    //Inicia os arquivos de script em Python
    initPyFiles();
}


/*
 * [Método]
 *
 * Descrição: Verifica se o banco de dados do Firebase
 * está vazio ou não.
 *
 * Parâmetros:
 *  - void
 */
bool FirebaseSync::isFirebaseDatabaseEmpty()
{
    //Coleta o retorno da requisição GET no Firebase
    QString output = getDatabaseFirebaseJSON("",GET_FIREBASE_NAME);
    //Verifica se é 'null'
    return output.contains("null");
}

/*
 * [Método]
 *
 * Descrição: Insere um dado, JSON estruturado, em um
 * determinado nó do Firebase.
 *
 * Parâmetros:
 *  - QString node : Nome do nó do Firebase
 *  - QString jsonData : Dado JSON estruturado
 */
QString FirebaseSync::insertIntoFirebaseDatabase(QString node, QString jsonData)
{
    //Envia os dados JSON para o Firebase (POST)
    return insertDatabaseFirebaseJSON(node, jsonData);
}

/*
 * [Método]
 *
 * Descrição: Retorna todos os dados contidos em um
 * determinado nó do Firebase.
 *
 * Parâmetros:
 *  - QString node : Nome do nó predeterminado
 */
QVariantMap FirebaseSync::getFirebaseDatabase(QString node)
{
    //Retorno da requisição GET no Firebase
    QString jsonData = getDatabaseFirebaseJSON(node, GET_FIREBASE_NAME);
    //Verifica se tá vazio
    if(jsonData.contains("null")) {
        qDebug() << "FIREBASE DATABASE IS EMPTY : " << jsonData;
        return QVariantMap();
    }
    //Traduz o retorno, JSON, em um objeto
    QJsonObject *mainObj = JsonTranslator::translate(jsonData.toLatin1());

    //Notifica que o banco recuperou os dados com sucesso
    qDebug() << "FIREBASE DATABASE RETRIEVED WITH SUCCESS";

    return mainObj->toVariantMap();
}

/*
 * [Método/Slot]
 *
 * Descrição: Recebe um novo ponto de métodos e o
 * envia para o Firebase
 *
 * Parâmetros:
 *  - MethodGraphPoint *point : novo ponto de métodos
 */
void FirebaseSync::hasNewMethodCalculated(MethodGraphPoint *point)
{
    //Cria um mapa de propriedades de métodos
    QVariantMap methodsProperties;
    //Adiciona os valores à esse mapa
    methodsProperties.insert("date", QDateTime::currentSecsSinceEpoch());
    methodsProperties.insert("thornwaite", point->get_thornwaite());
    methodsProperties.insert("camargo", point->get_camargo());
    methodsProperties.insert("penman_monteith", point->get_penmanmonteith());
    //Traduz esse mapa de propriedades para uma string em JSON
    QString json = JsonTranslator::translateToJson(methodsProperties);
    //Submete a string em JSON para o nó methods do Firebase
    QString ret = insertIntoFirebaseDatabase("methods", json);
    //Reporta que o método foi adicionado corretamente ao Firebase
    qDebug() << "NEW METHOD HAS BEEN ADDED TO FIREBASE :" << ret;
}

/*
 * [Método/Slot]
 *
 * Descrição: Recebe um novo estado do sistema
 * e o envia para o Firebase
 *
 * Parâmetros:
 *  - DBState *meanState : Novo estado do Sistema
 */
void FirebaseSync::hasNewMeanDataCalculated(DBState *meanState)
{
    //Cria um mapa de propriedades
    QVariantMap meansProperties;
    //Adiciona as propriedades do novo estado ao Mapa
    meansProperties.insert("date", QDateTime::currentSecsSinceEpoch());
    meansProperties.insert("wind_direction", meanState->get_windDirection());
    meansProperties.insert("wind_velocity", meanState->get_windVelocity());
    meansProperties.insert("temperature", meanState->get_temperature());
    meansProperties.insert("humidity", meanState->get_humidity());
    meansProperties.insert("accumulated_rain", meanState->get_accumulatedRain());
    meansProperties.insert("pressure_atm", meanState->get_pressureATM());
    meansProperties.insert("luminosity", meanState->get_luminosity());
    //Traduz esse mapa de propriedades em um arquivo JSON e o Submete para o Firebase
    QString ret = insertIntoFirebaseDatabase("means", JsonTranslator::translateToJson(meansProperties));
    qDebug() << "NEW MEAN HAS BEEN ADDED TO FIREBASE :" << ret;
}

/*
 * [Método]
 *
 * Descrição: Copia os arquivo, salvos
 * nos recursos do programa, como arquivos
 * locais, de forma que o sistema de arquivos
 * do sistema operacional consiga achá-los e
 * o interpretador do Python os execute.
 *
 * Parâmetros:
 *  - void
 */
void FirebaseSync::initPyFiles()
{
    //Carrega os endereços em arquivos
    QFile getF(GET_FIREBASE_PATH);
    QFile ngetF("getFromFirebase.py");
    QFile pushF(PUSH_FIREBASE_PATH);
    QFile npushF("insertIntoFirebase.py");

    //Copia o dado de um no outro
    getF.open(QIODevice::ReadOnly);
    QByteArray content =  getF.readAll();
    getF.close();

    ngetF.open(QIODevice::WriteOnly);
    ngetF.write(content);
    ngetF.close();

    //Reporta que a cópia foi bem realizada
    qDebug() << "INIT getFromFirebase.py FILE";

    pushF.open(QIODevice::ReadOnly);
    content =  pushF.readAll();
    pushF.close();

    npushF.open(QIODevice::WriteOnly);
    npushF.write(content);
    npushF.close();

    qDebug() << "INIT insertIntoFirebase.py FILE";
}

/*
 * [Método]
 *
 * Descrição: Executa um script em Python,
 * que faz uma requisição GET para o Firebase,
 * a qual retorna todos os dados de um determinado
 * nó.
 *
 * Parâmetros:
 *  - QString node : Nome do nó do Firebase
 *  - QString scriptPath : Caminho local do script
 */
QString FirebaseSync::getDatabaseFirebaseJSON(QString node, QString scriptPath) {

    //Instancia um processo do sistema
    QProcess *process = new QProcess(this);

    //Carrega uma lista de argumentos
    QStringList pythonCommandArguments =
            QStringList()
            << scriptPath
            << node;

    //Define as características do Processo
    process->setReadChannel(QProcess::StandardOutput);
    process->setProgram("python");
    process->setArguments(pythonCommandArguments);
    //Executa o processo
    process->start();
    //Aguarda o fim da execução
    process->waitForReadyRead();
    //Pega o retorno do processo
    QString output = QString(process->readAll());

    //Deleta o processo, quando possível
    process->deleteLater();
    return output;
}


/*
 * [Método]
 *
 * Descrição: Executa um script em Python,
 * que faz uma requisição POST para o Firebase,
 * a qual insere os dados de um determinado
 * nó.
 *
 * Parâmetros:
 *  - QString node : Nome do nó do Firebase
 *  - QString jsonData : Dados formatados em JSON
 */
QString FirebaseSync::insertDatabaseFirebaseJSON(QString node, QString jsonData) {
    //Instancia um Processo do sistema
    QProcess *process = new QProcess(this);

    //Cria uma lista de argumentos
    QStringList pythonCommandArguments =
            QStringList()
            << PUSH_FIREBASE_NAME
            << node
            << jsonData;

    //Define algumas características do Processo
    process->setReadChannel(QProcess::StandardOutput);
    process->setProgram("python");
    process->setArguments(pythonCommandArguments);
    //Executa o processo
    process->start();
    //Aguarda o término do mesmo
    process->waitForReadyRead();
    //Coleta o retorno do processo
    QString output = QString(process->readAll());

    //Deleta o processo, quando for possível
    process->deleteLater();
    return output;
}
