#ifndef WIFICONNECTION_H
#define WIFICONNECTION_H

#include <QObject>
#include <QTcpSocket>
#include <QTcpServer>

class WifiConnection : public QTcpServer
{
    Q_OBJECT
public:
    explicit WifiConnection(QObject *parent = 0);

signals:
    void readPort(QString string);
public slots:
    void slotNewConnection();
    void readData();

private:
    QTcpSocket* connection;
};

#endif // WIFICONNECTION_H
