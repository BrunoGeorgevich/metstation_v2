#ifndef JSONTRANSLATOR_H
#define JSONTRANSLATOR_H

#include <QDebug>

//Json Includes
#include <QJsonParseError>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

class JsonTranslator
{
public:
    static QJsonObject *translate(QByteArray data);
    static QString translateToJson(QVariantMap properties);
};

#endif // JSONTRANSLATOR_H
