#include "datamanager.h"

/*
 * [Construtor]
 *
 * Descrição: Executa o método construtor da
 * classe DataManager.
 *
 * Parâmetros:
 *  - QObject *parent : Ponteiro para o QObject Pai *
 */
DataManager::DataManager(QObject *parent) : QObject(parent)
{
    //Verifica se o banco do Firebase esá vazio ou não
    bool firebaseIsEmpty = initFirebase();

    //Inicia a comunicação com o LOCAL BD
    bool firstTime = initDB();
    //Verifica se é a primeira vez do Banco Local
    if(!firstTime)
        //Se não, carregar os dados salvos dele
        loadDataFromDB();
    else
        //Se sim, verificar se o banco do Firebase está vazio
        if(!firebaseIsEmpty)
            //Se não, baixar os dados dele
            loadDataFromFirebase();

    //Coletar o id do Log
    m_logId = m_db->select("logs","*").size() + 1;

    //Conectar o sinal que emite logs, com o
    //slot que os imprime na tela
    connect(m_db, SIGNAL(updateLog(QByteArray)),
            this, SLOT(log(QByteArray)));
}

/*
 * [Método]
 *
 * Descrição: Inicia o BD Local, criando
 * as tabelas dele.
 *
 * Parâmetros:
 *  - QObject *parent : Ponteiro para o QObject Pai *
 */
bool DataManager::initDB()
{
    //Carrega o BD database.db
    m_db = new Database("database.db");
    //Pega os nomes das tabelas
    QStringList tables = m_db->tables();
    //Define a variável que define se é
    //o primeiro vez
    bool firstTime = false;
    //Checa se existe a tabela de Logs,
    //se não, cria ela.
    if(!tables.contains("logs")) {
        //Criar Tabela de Logs
        QList<QByteArray> logsProperties;
        logsProperties.append("id int primary key not null");
        logsProperties.append("date int not null");
        logsProperties.append("log text not null");
        m_db->createTable("logs", logsProperties);

        firstTime = true;
    }
    //Checa se existe a tabela de Methods,
    //se não, cria ela.
    if(!tables.contains("methods")) {
        QList<QByteArray> methodsProperties;
        methodsProperties.append("id int primary key not null");
        methodsProperties.append("date int not null");
        methodsProperties.append("thornwaite real not null");
        methodsProperties.append("camargo real not null");
        methodsProperties.append("penman_monteith real not null");
        m_db->createTable("methods", methodsProperties);
    }
    //Checa se existe a tabela de Means,
    //se não, cria ela.
    if(!tables.contains("means")) {
        QList<QByteArray> meansProperties;
        meansProperties.append("id int primary key not null");
        meansProperties.append("date int not null");
        meansProperties.append("wind_direction int not null");
        meansProperties.append("wind_velocity real not null");
        meansProperties.append("temperature real not null");
        meansProperties.append("humidity real not null");
        meansProperties.append("accumulated_rain real not null");
        meansProperties.append("pressure_atm real not null");
        meansProperties.append("luminosity real not null");
        m_db->createTable("means", meansProperties);
    }
    //Checa se existe a tabela de Data,
    //se não, cria ela.
    if(!tables.contains("data")) {
        QList<QByteArray> dataProperties;
        dataProperties.append("id int primary key not null");
        dataProperties.append("date int not null");
        dataProperties.append("wind_direction int not null");
        dataProperties.append("wind_velocity real not null");
        dataProperties.append("temperature real not null");
        dataProperties.append("humidity real not null");
        dataProperties.append("accumulated_rain real not null");
        dataProperties.append("pressure_atm real not null");
        dataProperties.append("luminosity real not null");
        m_db->createTable("data", dataProperties);
    }

    return firstTime;
}

/*
 * [Método]
 *
 * Descrição: Inicia o BD Remoto, Firebase,
 * criando as tabelas dele.
 *
 * Parâmetros:
 *  - void
 */
bool DataManager::initFirebase()
{
    //Instancia a sincronizaçao com o Firebase
    m_fb = new FirebaseSync(this);

    //Conecta os sinais do Firebase com o respectivos Slots
    connect(this, SIGNAL(newMeanDataCalculated(DBState*)),
            m_fb, SLOT(hasNewMeanDataCalculated(DBState*)));
    connect(this, SIGNAL(newMethodsCalculated(MethodGraphPoint*)),
            m_fb, SLOT(hasNewMethodCalculated(MethodGraphPoint*)));

    return m_fb->isFirebaseDatabaseEmpty();
}

/*
 * [Método]
 *
 * Descrição: Apaga os dados da tabela Data.
 *
 * Parâmetros:
 *  - void
 */
void DataManager::loadDataFromDB()
{
    m_db->deleteFrom("data","id > -1");
}

/*
 * [Método]
 *
 * Descrição: Carrega os dados salvos no BD remoto,
 * Firebase.
 *
 * Parâmetros:
 *  - void
 */
void DataManager::loadDataFromFirebase()
{
    //Recebe o mapa dos últimos 30 dados do Firebase, na tabela means
    QVariantMap fbMeansMap = m_fb->getFirebaseDatabase("means");
    //Itera entre os dados do Firebase, nó 'means', e carrega eles
    //em uma variável local
    foreach (QString key, fbMeansMap.keys()) {
        QVariantMap varMap = fbMeansMap.value(key).toMap();
        saveMeanDataMetStation(new DBState(varMap,this), false);
    }
    //Recebe o mapa dos últimos 30 dados do Firebase, na tabela methods
    QVariantMap fbMethodsMap = m_fb->getFirebaseDatabase("methods");
    //Itera entre os dados do Firebase, nó 'methods', e carrega eles
    //em uma variável local
    foreach (QString key, fbMethodsMap.keys()) {
        QVariantMap varMap = fbMethodsMap.value(key).toMap();
        saveMethodsResults(new MethodGraphPoint(
                               varMap.value("thornwaite").toFloat(),
                               varMap.value("camargo").toFloat(),
                               varMap.value("penman_monteith").toFloat(),
                               this
                               )
                           );
    }
}

/*
 * [Método]
 *
 * Descrição: Carrega os dados salvos no BD local
 * e faz a média deles, em um número determinado
 * de dias.
 *
 * Parâmetros:
 *  - int days : Número determinado de dias
 */
QVector<float> DataManager::getAverageLastDays(int days)
{
    //Cria um vetor de médias
    QVector<float> means;

    //Adiciona médias 0 no vetor
    for (int i = 0; i < 8; ++i) means.append(0);

    //Carrega os dados do Banco de Dados Local
    QVariantList list = m_db->select("means","*");
    //Verifica o tamanho dos dados
    int count = list.size();
    count = (count >= days) ? days : count;
    //Itera entre os dados do Banco
    for (int i = 0; i < count; ++i) {
        QVariantMap map = list.at(i).toMap();
        means[WINDDIRECTION - 1] += map.value("wind_direction").toInt();
        means[WINDVELOCITY - 1] += map.value("wind_velocity").toFloat();
        means[TEMPERATURE - 1] += map.value("temperature").toFloat();
        means[HUMIDITY - 1] += map.value("humidity").toFloat();
        means[ACCUMULATEDRAIN - 1] += map.value("accumulated_rain").toFloat();
        means[PRESSUREATM- 1] += map.value("pressure_atm").toFloat();
        means[LUMINOSITY - 1] += map.value("luminosity").toFloat();
    }

    //Divide todos os dados pelo tamanho da lista,
    //realizando a média aritmética de cada um
    for (int i = 0; i < 8; ++i) means[i] /= count;

    return means;
}


/*
 * [Método]
 *
 * Descrição: Salva as médias provenientes da
 * estação metereológica, nos bancos de dados.
 *
 * Parâmetros:
 *  - DBState *meanState : Estado médio a ser salvo
 *  - bool emitToFirebase : Define se o dado será salvo no Firebase
 */
void DataManager::saveMeanDataMetStation(DBState *meanState, bool emitToFirebase)
{
    //Cria um mapa de valores
    QMap<QString,QString> values;
    //Atribui os dados a esse mapa
    values.insert("id",QString::number(m_db->select("means","*").size()));
    values.insert("date",QString::number(QDateTime::currentSecsSinceEpoch()));
    values.insert("wind_direction",QString::number(meanState->get_windDirection()));
    values.insert("wind_velocity",QString::number(meanState->get_windVelocity()));
    values.insert("temperature",QString::number(meanState->get_temperature()));
    values.insert("humidity",QString::number(meanState->get_humidity()));
    values.insert("accumulated_rain",QString::number(meanState->get_accumulatedRain()));
    values.insert("pressure_atm",QString::number(meanState->get_pressureATM()));
    values.insert("luminosity",QString::number(meanState->get_luminosity()));
    //Insere esse mapa no BD local
    m_db->insertInto("means",values);

    //Caso sim, envia o mapa para o Firebase
    if(emitToFirebase)
        emit newMeanDataCalculated(meanState);

    //Deleta todos os dados da tabela Data
    m_db->deleteFrom("data","id > -1");
    //Deleta todos os dados da tabela Logs
    if(m_db->deleteFrom("logs","id > -1"))
        m_logId = 0;
}

/*
 * [Método]
 *
 * Descrição: Carrega os dados salvos no BD remoto,
 * Firebase.
 *
 * Parâmetros:
 *  - void
 */
void DataManager::saveMethodsResults()
{
    //Salva um log
    addLogToDB("NEW METHODS RESULTS SAVED");
    //Calcula os devidos métodos para os dados coletados nos 3 dias
    QVector<float> data = getAverageLastDays(3);
    float method1 = MethodsEvapotranspiration::getInstance()->getThornthwaite(data.at(TEMPERATURE),27.5);
    float method2 = MethodsEvapotranspiration::getInstance()->getCamargo(data.at(TEMPERATURE));
    float method3 = MethodsEvapotranspiration::getInstance()->getPenmanMonteith(data.at(TEMPERATURE), data.at(HUMIDITY), data.at(PRESSUREATM), data.at(WINDVELOCITY));
    //Instancia um ponto de métodos novo
    MethodGraphPoint *mgPoint = new MethodGraphPoint(method1,method2, method3, this);
    //Salva esse ponto nos bancos de Dados
    saveMethodsResults(mgPoint);
    //Envia um dado de ponto de métodos novo
    emit newMethodsCalculated(mgPoint);
}

/*
 * [Método]
 *
 * Descrição: Salva os resultados de um ponto de métodos no
 * banco de dados Local
 *
 * Parâmetros:
 *  - MethodGraphPoint *mgPoint : ponto de métodos novo
 */
void DataManager::saveMethodsResults(MethodGraphPoint *mgPoint)
{
    //Cria um mapa de valores
    QMap<QString,QString> values;
    //Insere os valores no mapa
    values.insert("id",QString::number(m_db->select("methods","*").size()));
    values.insert("date",QString::number(QDateTime::currentSecsSinceEpoch()));
    values.insert("thornwaite",QString::number(mgPoint->get_thornwaite()));
    values.insert("camargo",QString::number(mgPoint->get_camargo()));
    values.insert("penman_monteith",QString::number(mgPoint->get_penmanmonteith()));
    //Insere o mapa de valores no BD Local
    m_db->insertInto("methods",values);
}

/*
 * [Método]
 *
 * Descrição: Salva um novo estado no BD Local
 *
 * Parâmetros:
 *  - DBState *state : Novo estado
 */
void DataManager::addStateToDB(DBState *state)
{
    //Adiciona um log do sistema
    addLogToDB("RECEIVED NEW DB STATE");
    //Cria um mapa de valores
    QMap<QString, QString> values;
    //Insere os dados no mapa
    values.insert("id",QString::number(state->get_version()));
    values.insert("date",QString::number(state->get_timeStamp().toSecsSinceEpoch()));
    values.insert("wind_direction",QString::number(state->get_windDirection()));
    values.insert("wind_velocity",QString::number(state->get_windVelocity()));
    values.insert("temperature",QString::number(state->get_temperature()));
    values.insert("humidity",QString::number(state->get_humidity()));
    values.insert("accumulated_rain",QString::number(state->get_accumulatedRain()));
    values.insert("pressure_atm",QString::number(state->get_pressureATM()));
    values.insert("luminosity",QString::number(state->get_luminosity()));
    //Adiciona o mapa na tabela Data
    m_db->insertInto("data",values);
}

/*
 * [Método]
 *
 * Descrição: Adiciona os logs do sistema na
 * tabela de logs.
 *
 * Parâmetros:
 *  - QByteArray log : String do log
 */
void DataManager::addLogToDB(QByteArray log)
{
    //Cria um mapa de propriedades
    QMap<QString, QString> properties;
    //Insere os dados no mapa
    properties.insert("id", QString::number(m_logId));
    properties.insert("date", QString::number(QDateTime::currentSecsSinceEpoch()));
    properties.insert("log", QString("\'" + log + "\'"));
    //Insere o log na tabela de Logs
    if(m_db->insertInto("logs", properties))
        //Incrementa o ID do último log
        m_logId++;
}

/*
 * [Método]
 *
 * Descrição: Recupera do banco de dados Local os
 * últimos 30 pontos de métodos.
 *
 * Parâmetros:
 *  - void
 */
QList<MethodGraphPoint *> DataManager::retrieveLastMethodsPoints()
{
    //Recupera todos os dados da tabela methods em uma lista
    QVariantList dbMethods = m_db->select("methods", "*");
    //Cria uma lista de pontos de métodos
    QList<MethodGraphPoint *> lastMethods;
    //Reduz o tamanho da lista de dados em 30 itens
    while(dbMethods.size() > 30) {
        dbMethods.removeFirst();
    }
    //Itera entre os últimos 30 itens e os adiciona
    //na nova lista de pontos de métodos
    foreach (QVariant var, dbMethods) {
        QVariantMap map = var.toMap();
        float t = map.value("thornwaite").toFloat();
        float c = map.value("camargo").toFloat();
        float p = map.value("penmanmonteith").toFloat();
        lastMethods.append(new MethodGraphPoint(t,p,c));
    }
    return lastMethods;
}

/*
 * [Método]
 *
 * Descrição: Retorna os últimos 30 estados do sistema.
 *
 * Parâmetros:
 *  - void
 */
QList<DBState *> DataManager::retrieveLastStatePoints()
{
    //Recupera todos os dados da tabela 'means' em uma lista
    QVariantList dbStates = m_db->select("means", "*");
    //Cria uma lista de estados
    QList<DBState *> lastStates;
    //Reduz a lista de dados em 30 itens
    while(dbStates.size() > 30) {
        dbStates.removeFirst();
    }
    //Itera entre os últimos 30 itens e
    //os adiciona na lista de estados
    foreach (QVariant var, dbStates) {
        QVariantMap map = var.toMap();
        lastStates.append(new DBState(map));
    }
    return lastStates;
}


/*
 * [Método/Slot]
 *
 * Descrição: Imprime os logs enviados
 *
 * Parâmetros:
 *  - QByteArray log : String com os dados dos Logs
 */
void DataManager::log(QByteArray log)
{
    qDebug() << log;
}
