#ifndef DATAMANAGER_H
#define DATAMANAGER_H

#include <QObject>

#include "dbstate.h"

#include "../Controller/definitions.h"
#include "../Controller/methodgraphpoint.h"
#include "../Controller/methodsEvapotranspiration.h"

#include "../Essencials/database.h"

#include "firebasesync.h"

class DataManager : public QObject
{
    Q_OBJECT
public:
    explicit DataManager(QObject *parent = 0);
    QVector<float> getAverageLastDays(int days);
    void saveMeanDataMetStation(DBState *meanState, bool emitToFirebase = true);
    void saveMethodsResults();
    void addStateToDB(DBState *state);
    void addLogToDB(QByteArray log);
    QList<MethodGraphPoint *> retrieveLastMethodsPoints();
    QList<DBState *> retrieveLastStatePoints();
private slots:
    void log(QByteArray log);
signals:
    void newMethodsCalculated(MethodGraphPoint *point);
    void newMeanDataCalculated(DBState *meanState);
private:
    Database *m_db;
    FirebaseSync *m_fb;

    void saveMethodsResults(MethodGraphPoint *mgPoint);

    int m_logId;
    bool initDB();
    bool initFirebase();
    void loadDataFromDB();
    void loadDataFromFirebase();
};

#endif // DATAMANAGER_H
