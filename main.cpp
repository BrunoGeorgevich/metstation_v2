#include <QQmlApplicationEngine>
#include <QApplication>
#include <QQmlContext>

#include "Controller/controller.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QQmlApplicationEngine engine;
    QQmlContext *ctx = engine.rootContext();

    Controller *controller = new Controller();
    ctx->setContextProperty("controller", controller);

    engine.load(QUrl("qrc:/main.qml"));
    return a.exec();
}
