#-------------------------------------------------
#
# Project created by QtCreator 2016-04-04T18:48:26
#
#-------------------------------------------------

QT       += core gui sql qml quick
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = metStation
TEMPLATE = app

SOURCES += main.cpp                          \
    Model/datacollector.cpp                  \
    Model/jsontranslator.cpp                 \
    Model/datamanager.cpp                    \
    Model/dbstate.cpp                        \
    Model/wificonnection.cpp                 \
    Model/firebasesync.cpp                   \
    Essencials/database.cpp                  \
    Essencials/qqmlhelpers.cpp               \
    Essencials/qqmlobjectlistmodel.cpp       \
    Controller/methodsEvapotranspiration.cpp \
    Controller/timer.cpp                     \
    Controller/elapsedtime.cpp               \
    Controller/controller.cpp                \
    Controller/dataprovider.cpp              \
    Controller/methodgraphpoint.cpp

HEADERS  +=                                  \
    Model/datacollector.h                    \
    Model/jsontranslator.h                   \
    Model/datamanager.h                      \
    Model/dbstate.h                          \
    Model/wificonnection.h                   \
    Model/firebasesync.h                     \
    Essencials/database.h                    \
    Essencials/qqmlhelpers.h                 \
    Essencials/qqmlobjectlistmodel.h         \
    Controller/dataprovider.h                \
    Controller/methodgraphpoint.h            \
    Controller/graphdetails.h                \
    Controller/timer.h                       \
    Controller/elapsedtime.h                 \
    Controller/definitions.h                 \
    Controller/controller.h                  \
    Controller/methodsEvapotranspiration.h

RC_FILE = assets/applicationIcon.rc

RESOURCES += \
    assets/myresource.qrc \
    assets/qml.qrc
