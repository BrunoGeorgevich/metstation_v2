Quando fizer o clone, em seguida execute a seguinte linha de código na 
pasta root do projeto:
	
	git submodule init
	git submodule update

Dependências:
	
	python3 -> install python3-dev
		Debian-based: sudo apt install python3-dev python3-pip
		Arch-based: sudo pacman -S python3-dev python3-pip
	Atualizar pip:
		sudo pip install --upgrade pip
	pyrebase -> 
		sudo pip install pyrebase
