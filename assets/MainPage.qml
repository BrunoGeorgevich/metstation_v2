import QtQuick 2.9
import QtQuick.Controls 2.2

Page {
    id: firstPage
    background: Rectangle { color:"transparent" }
    Item {
        id:grid
        property real spacingH: parent.width*0.05
        property real spacingV: parent.height*0.05

        anchors {
            left:parent.left; leftMargin: spacingH
            right:parent.right; /*rightMargin: spacingH*/
            top:parent.top; topMargin: spacingV
            bottom:parent.bottom; /*bottomMargin: spacingV*/
        }

        Column {
            id:mainColumn
            property real cellWidth: (parent.width/3) - grid.spacingH
            property real cellHeight: (parent.height/3) - grid.spacingV
            anchors.fill: parent
            spacing: grid.spacingV

            Row {
                id:firstRow
                spacing: grid.spacingH
                Compass {
                    id:compass
                    width: mainColumn.cellWidth; height: mainColumn.cellHeight

                    angle: controller.dp.currentState.windDirection
                }
                GridCell {
                    id:windVelocity
                    width: mainColumn.cellWidth; height: mainColumn.cellHeight
                    titleText: "Wind Velocity"
                    dataText: controller.dp.currentState.windVelocity
                    unitText: "m/s"
                }
                GridCell {
                    id:temperature
                    width: mainColumn.cellWidth; height: mainColumn.cellHeight
                    titleText: "Temperature"
                    dataText: controller.dp.currentState.temperature
                    unitText: " °C"
                }
            }

            Row {
                id:secondRow
                spacing: grid.spacingH
                GridCell {
                    id:accumulatedRain
                    width: mainColumn.cellWidth; height: mainColumn.cellHeight
                    titleText: "Accumulated Rain"
                    dataText: controller.dp.currentState.accumulatedRain
                    unitText: " mm/h"
                }
                GridCell {
                    id:pressureATM
                    width: mainColumn.cellWidth; height: mainColumn.cellHeight
                    titleText: "ATM Pressure"
                    dataText: controller.dp.currentState.pressureATM
                    unitText: " kPa"
                }
                GridCell {
                    id:humidity
                    width: mainColumn.cellWidth; height: mainColumn.cellHeight
                    titleText: "Humidity"
                    dataText: controller.dp.currentState.humidity
                    unitText: " %"
                }
            }

            Row {
                id:thirdRow
                spacing: grid.spacingH
                GridCell {
                    id:luminosity
                    width: mainColumn.cellWidth; height: mainColumn.cellHeight
                    titleText: "Luminosity"
                    dataText: controller.dp.currentState.luminosity
                    unitText: " lux"
                }
//                GridCell {
//                    id:battery
//                    width: mainColumn.cellWidth; height: mainColumn.cellHeight
//                    titleText: "Battery"
//                    dataText: controller.dp.currentState.battery
//                    unitText: " v"
//                }
            }
        }
    }

}
