import QtQuick 2.9
import QtCharts 2.2
import QtQuick.Controls 2.2

Item {
    ChartView {
        id:chart
        function update() {
            penmanGraph.clear(); thornwaiteGraph.clear(); camargoGraph.clear();
            for(var i = 0; i < controller.dp.methods.size(); i++) {
                penmanGraph.append(i, controller.dp.methods.get(i).penmanmonteith)
                thornwaiteGraph.append(i, controller.dp.methods.get(i).thornwaite)
                camargoGraph.append(i, controller.dp.methods.get(i).camargo)
            }
            updateAxis()
            updateTitle()
        }
        function updateAxis() {
            axisY.max = controller.dp.mDetails.majorValue*(1.1)
            axisX.max = controller.dp.methods.size() - 1
        }
        function updateTitle() {
            chart.title = "Methods (" + controller.dp.methods.size() + " Days)"
        }
        anchors.fill: parent
        antialiasing: true

        Connections {
            target:controller.dp
            onUpdateMethodsChart: {
//                console.log("ITEM WAS ADDED TO METHODS LIST")
                chart.update()
            }
            onMethodsChartDetailsHasChanged: {
//                console.log("DETAILS HAS CHANGED!")
                chart.updateAxis()
            }
        }

        ValueAxis {
            id: axisX
            min: 0
            max: 30
            tickCount: 7
        }

        ValueAxis {
            id: axisY
            min: 0
            max: 100
            tickCount: 7
        }

        LineSeries {
            id:penmanGraph
            axisX: axisX
            axisY: axisY
            name: "Penman-Monteith"
            color:"Blue"
        }
        LineSeries {
            id:thornwaiteGraph
            axisX: axisX
            axisY: axisY
            name: "Thornwaite"
            color:"Red"
        }
        LineSeries {
            id:camargoGraph
            axisX: axisX
            axisY: axisY
            name: "Camargo"
            color:"Green"
        }

        Component.onCompleted: {
            update()
        }
    }

    Button {
        text:"New Method"
        onClicked: controller.dp.addMethodPoint()
    }
}
