import QtQuick 2.0
import QtQuick.Controls 2.1
import QtGraphicalEffects 1.0

Item {

    property alias titleText: title.text
    property alias dataText: data.text
    property alias unitText: unit.text

    Rectangle {
        id:rectArea
        width: parent.width; height: parent.height
        radius:height/25
        color:"white"
        Column {
            anchors.fill: parent
            Label {
                id:title
                width:parent.width; height: parent.height*(1/5)
                horizontalAlignment: "AlignHCenter"; verticalAlignment: "AlignVCenter"
                fontSizeMode: Label.Fit; font.pointSize: 35; text:"TITLE"
            }
            Item {
                width:parent.width; height: parent.height*(4/5)
                Label {
                    id:data
                    width:parent.width; height: parent.height*(4/5)
                    horizontalAlignment: "AlignHCenter"; verticalAlignment: "AlignVCenter"
                    fontSizeMode: Label.Fit; font.pointSize: 35; text:"DATA"
                }
                Label {
                    id:unit
                    width:parent.width*(1/5); height: parent.height*(4/5)
                    anchors { right:parent.right; bottom: parent.bottom; margins: parent.width*(0.1) }
                    horizontalAlignment: "AlignHCenter"; verticalAlignment: "AlignBottom"
                    fontSizeMode: Label.Fit; font.pointSize: 35; text:"unit"
                }
            }
        }
    }
    DropShadow {
        id:shadowArea
        anchors.fill: rectArea
        horizontalOffset: 1
        verticalOffset: 1
        radius: 8.0
        samples: 17
        color: "#A0000000"
        transparentBorder: true
        source: rectArea
    }
}
