import QtQuick 2.0
import QtQuick.Controls 2.1

Item {
    id:compassRoot
    property real spacing: height*0.15
    property real angle: 0
    Rectangle {
        id:backCircle
        anchors.centerIn: parent
        width: parent.height; height: width; radius: width/2
        color:"#777"
    }
    Rectangle {
        id:centerCircle
        anchors.centerIn: parent
        width: backCircle.height - spacing; height: width; radius: width/2
        color:"#9F9F9F"
        Item {
            anchors {
                left:parent.left; right:parent.right;
                top:parent.top; bottom:parent.bottom;
                margins: 5
            }
            Label {
                anchors { top: parent.top; horizontalCenter: parent.horizontalCenter }
                text:"N"; font { pointSize: 15 }
                color: "White"
            }
            Label {
                anchors { bottom: parent.bottom; horizontalCenter: parent.horizontalCenter }
                text:"S"; font { pointSize: 15 }
                color: "White"
            }
            Label {
                anchors { left: parent.left; verticalCenter: parent.verticalCenter }
                text:"O"; font { pointSize: 15 }
                color: "White"
            }
            Label {
                anchors { right: parent.right; verticalCenter: parent.verticalCenter }
                text:"L"; font { pointSize: 15 }
                color: "White"
            }
        }
    }
    Item {
        anchors.fill: centerCircle
        transform: Rotation {
                    id:rotationCenterRect
                    axis { x:0;y:0;z:1 }
                    angle:compassRoot.angle - 180

                    origin.x:compass.centreX
                    origin.y:compass.centreY
                }
        Canvas {
            id:compass

            property real centreX : width / 2;
            property real centreY : height / 2;

            anchors.fill: parent
            onPaint: {
                var ctx = getContext("2d");
                ctx.reset();
                var topColor = "#E55"
                var bottomColor = "#EEE"
                var compassWidth = parent.width*0.7
                var compassHeight = parent.width/8

                ctx.strokeStyle = "#333"
                ctx.lineWidth = 0.5

                //Draw the top side
                ctx.beginPath();
                ctx.fillStyle = topColor
                ctx.moveTo(centreX - compassWidth/2, centreY)
                .lineTo(centreX*0.9, centreY - compassHeight/2)
                .lineTo(centreX*1.1, centreY + compassHeight/2)
                .lineTo(centreX - compassWidth/2, centreY)
                ctx.fill();
                ctx.stroke();

                //Draw the gray side
                ctx.beginPath();
                ctx.fillStyle = bottomColor
                ctx.moveTo(centreX*0.9, centreY - compassHeight/2)
                .lineTo(centreX + compassWidth/2, centreY)
                .lineTo(centreX*1.1, centreY + compassHeight/2);
                ctx.fill();
                ctx.stroke();

            }
        }
    }
}
