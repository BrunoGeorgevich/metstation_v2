import QtQuick 2.9
import QtQuick.Controls 2.1
import QtCharts 2.2

ApplicationWindow {
    id:root
    title: "Estação Metereológica - EASY"
    visible: true
    width: 800
    height: 480
    Rectangle {
        id:backgroudRect
        anchors.fill: parent
        color:"#DADADA"
    }

    SwipeView {
        id: view

        currentIndex: 0
        anchors.fill: parent

        MainPage { }
        MethodsGraphsPage { }
        VariablesGraphPage { }
    }

    PageIndicator {
        id: indicator

        count: view.count
        currentIndex: view.currentIndex

        anchors.bottom: view.bottom
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
