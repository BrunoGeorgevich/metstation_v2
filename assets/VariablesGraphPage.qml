import QtQuick 2.9
import QtCharts 2.2
import QtQuick.Controls 2.2

Item {
    id: thirdPage
    Rectangle {
        id:topButtonBar
        color:"#838383"
        width:parent.width; height: parent.height*(0.1)
        Row {
            anchors.fill: parent
            spacing: parent.width*(0.002)
            ImageButton {
                source:"qrc:/Images/temperature.png"
                mouseArea.onClicked: {
                    chart._type = "temperature"
                    chart.update()
                }
            }
            ImageButton {
                source:"qrc:/Images/sun.png"
                mouseArea.onClicked: {
                    chart._type = "luminosity"
                    chart.update()
                }
            }
            ImageButton {
                source:"qrc:/Images/rain.png"
                mouseArea.onClicked: {
                    chart._type = "accumulatedRain"
                    chart.update()
                }
            }
            ImageButton {
                source:"qrc:/Images/humidity.png"
                mouseArea.onClicked: {
                    chart._type = "humidity"
                    chart.update()
                }
            }
            ImageButton {
                source:"qrc:/Images/wind.png"
                mouseArea.onClicked: {
                    chart._type = "windVelocity"
                    chart.update()
                }
            }
        }
    }

    ChartView {
        id:chart

        property string _type: "temperature"
        function update() {

            chartSerie.clear()

            switch(_type) {
            case "temperature":
                chart.title = "Temperature"
                chartSerie.color = "red"
                chartSerie.name = "°C"
                break;
            case "humidity":
                chart.title = "Humidity"
                chartSerie.color = "blue"
                chartSerie.name = "%"
                break;
            case "luminosity":
                chart.title = "Luminosity"
                chartSerie.color = "green"
                chartSerie.name = "lux"
                break;
            case "windVelocity":
                chart.title = "Wind Velocity"
                chartSerie.color = "yellow"
                chartSerie.name = "m/s"
                break;
            case "accumulatedRain":
                chart.title = "Accumulated Rain"
                chartSerie.color = "darkblue"
                chartSerie.name = "mm/h"
                break;
            default:
            }

            for(var i = 0; i < controller.dp.states.size(); i++) {
                switch(_type) {
                case "temperature":
                    chartSerie.append(i, controller.dp.states.get(i).temperature)
                    break;
                case "humidity":
                    chartSerie.append(i, controller.dp.states.get(i).humidity)
                    break;
                case "luminosity":
                    chartSerie.append(i, controller.dp.states.get(i).luminosity)
                    break;
                case "windVelocity":
                    chartSerie.append(i, controller.dp.states.get(i).windVelocity)
                    break;
                case "accumulatedRain":
                    chartSerie.append(i, controller.dp.states.get(i).accumulatedRain)
                    break;
                default:
                    console.log("GUI_ERR 01 : ENTER INTO DEFAULT!")
                }
            }

            updateAxis();
        }        
        function updateAxis() {
            switch(_type) {
            case "temperature":
                axisY.max = controller.dp.sDetails.get(0).majorValue*(1.1)
                break;
            case "luminosity":
                axisY.max = controller.dp.sDetails.get(1).majorValue*(1.1)
                break;
            case "accumulatedRain":
                axisY.max = controller.dp.sDetails.get(2).majorValue*(1.1)
                break;
            case "humidity":
                axisY.max = controller.dp.sDetails.get(3).majorValue*(1.1)
                break;
            case "windVelocity":
                axisY.max = controller.dp.sDetails.get(4).majorValue*(1.1)
                break;
            default:
                console.log("GUI_ERR 02 : ENTER INTO DEFAULT!")
            }
            axisX.max = controller.dp.states.size() - 1
        }

        anchors {
            top:topButtonBar.bottom; left:parent.left;
            right:parent.right; bottom: parent.bottom;
        }
        title: "Line"; antialiasing: true


        Connections {
            target:controller.dp
            onUpdateStatesChart: {
//                console.log("ITEM WAS ADDED TO STATES LIST")
                chart.update()
            }
            onStatesChartDetailsHasChanged: {
//                console.log("DETAILS HAS CHANGED!")
                chart.updateAxis()
            }
        }

        ValueAxis {
            id: axisX
            min: 0
            max: 30
            tickCount: 7
        }

        ValueAxis {
            id: axisY
            min: 0
            max: 100
            tickCount: 7
        }

        LineSeries {
            id:chartSerie
            axisX: axisX
            axisY: axisY
            name: "LineSeries"
        }
    }
    Component.onCompleted: chart.update()
}
