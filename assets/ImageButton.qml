import QtQuick 2.9
import QtQuick.Controls 2.2

Item {
    property alias source: implicitImage.source
    property alias mouseArea: m_mouseArea

    height:parent.height; width:height
    Image {
        id:implicitImage
        anchors.centerIn: parent
        mipmap: true; antialiasing: true; smooth: true
        height:parent.height*(0.75); width: height
        fillMode: Image.PreserveAspectFit
    }

    MouseArea {
        id: m_mouseArea
        anchors.fill: parent
    }
}
