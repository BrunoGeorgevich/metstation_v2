#ifndef DATAPROVIDER_H
#define DATAPROVIDER_H

#include <QObject>

#include "../Model/datamanager.h"

#include "methodgraphpoint.h"
#include "graphdetails.h"

#include "../Model/dbstate.h"

class DataProvider : public QObject
{
    Q_OBJECT

    QML_READONLY_PROPERTY(DBState *, currentState)

    QML_READONLY_PROPERTY(GraphDetails *, mDetails)
    QML_OBJMODEL_PROPERTY(GraphDetails, sDetails)

    QML_OBJMODEL_PROPERTY(DBState, states)
    QML_OBJMODEL_PROPERTY(MethodGraphPoint, methods)

public:
    explicit DataProvider(DataManager *dm, DBState *initialState, QObject *parent = nullptr);
signals:
    void updateMethodsChart();
    void updateStatesChart();
    void methodsChartDetailsHasChanged(GraphDetails *details);
    void statesChartDetailsHasChanged(QQmlObjectListModel<GraphDetails> *details);
private slots:
    void changeCurrentState(DBState *newState);
    void addMethodsPoint(MethodGraphPoint *newPoint);
private:
    void initStateList(DBState *initialState);
    void initMethodsList();

    float verifyIfMaximumOfMethodGraphHasChanged(MethodGraphPoint *mgPoint);
    bool verifyMaximumOfStatesGraphHaschanged(DBState *state, int index);
    void updateMaximumOfMethodGraph();
    void updateMaximumOfStatesGraph();

    DataManager *m_dm;
    QMap<QByteArray, GraphDetails *> toMap();
    void fromMap(QMap<QByteArray, GraphDetails *> map);
};

#endif // DATAPROVIDER_H
