#ifndef METHODGRAPHPOINT_H
#define METHODGRAPHPOINT_H

#include <QObject>

#include "../Essencials/qqmlhelpers.h"

class MethodGraphPoint : public QObject
{
    Q_OBJECT
    QML_WRITABLE_PROPERTY(float, thornwaite)
    QML_WRITABLE_PROPERTY(float, penmanmonteith)
    QML_WRITABLE_PROPERTY(float, camargo)
public:
    explicit MethodGraphPoint(float t, float p, float c, QObject *parent = nullptr);
};

#endif // METHODGRAPHPOINT_H
