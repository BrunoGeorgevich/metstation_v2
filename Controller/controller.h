#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <QObject>
#include <QTimer>
#include <QDebug>

#include "timer.h"
#include "definitions.h"
#include "methodsEvapotranspiration.h"
#include "dataprovider.h"

#include "../Essencials/database.h"
#include "../Essencials/qqmlhelpers.h"
#include "../Essencials/qqmlobjectlistmodel.h"

#include "../Model/datacollector.h"
#include "../Model/datamanager.h"

class Controller : public QObject
{
    Q_OBJECT
    QML_READONLY_PROPERTY(DataProvider *, dp)
public:
    explicit Controller(QObject *parent = 0);
private slots:
    void storesStationData();
    void receivedNewRemoteDBState(DBState *state);
private:
    void addMeanState(DBState *newState);
    void realizeMean();

    DataCollector *m_dc;
    DataManager *m_dm;
    Timer *m_timer;
    DBState *m_meanState;
    int m_numDays;
    int m_numSamples;
};

#endif // CONTROLLER_H
