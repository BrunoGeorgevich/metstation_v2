#include "timer.h"

/*
 * [Construtor]
 *
 * Descrição: Executa o método construtor da
 * classe Timer.
 *
 * Parâmetros:
 *  - int intervalMsec : Define o tamanho do intervalo de
 *    contagem do cronômetro, em milissegundos
 *  - QObject *parent : Ponteiro para o QObject Pai *
 */
Timer::Timer(int intervalMsec, QObject *parent) : QObject(parent)
{
    //Instancia o timer
    m_timer = new QTimer(this);
    //Atribui o tamanho predeterminado do intervalo
    m_interval = intervalMsec;
    //Instancia o contador de tempo passado
    m_elapsedTime = new ElapsedTime(this);
    //Conecta o fim do ciclo do timer com o slot timeoutComplete
    connect(m_timer, SIGNAL(timeout()),
            this, SLOT(timeoutComplete()));
    //Inicia o timer
    m_timer->start(m_interval);
}

/*
 * [Método/Slot]
 *
 * Descrição: Ordena que o contador de tempo passado incremente
 * mais um dia.
 *
 * Parâmetros:
 *  - void
 */
void Timer::timeoutComplete()
{
        m_elapsedTime->addMinute(m_interval, m_interval/3);
}

/*
 * [Método/Slot]
 *
 * Descrição: Retorna um ponteiro para o contador de dias passados.
 *
 * Parâmetros:
 *  - void
 */
ElapsedTime *Timer::elapsedTime() const
{
    return m_elapsedTime;
}

