#ifndef METHODSEVAPOTRANSPIRATION_H
#define METHODSEVAPOTRANSPIRATION_H

#include <QVariantMap>
#include <QObject>
#include <QDebug>
#include <QDate>

#include <math.h>

#define ThreeDays 3 // Cálculo do método para 3 dias

class MethodsEvapotranspiration : public QObject
{
    Q_OBJECT
public:
    static MethodsEvapotranspiration *getInstance();
    float getThornthwaite(float avarageTemp, float yearTemp);
    float getCamargo(float avarageTemp);
    float getPenmanMonteith(float avarageTemp, float humidity, float atmPressure, float windSpeed);
    float getSolarRadiantion(QString month);
    float getPhotoPeriod(QString month);
private:
    MethodsEvapotranspiration(QObject *parent = 0);
    void initSolarRadiation();
    void initPhotoPeriod();

    static MethodsEvapotranspiration *m_instance;
    QVariantMap m_solarRadiation;
    QVariantMap m_photoPeriod;
};

#endif // METHODSEVAPOTRANSPIRATION_H
