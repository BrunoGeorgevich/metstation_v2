#include "elapsedtime.h"

/*
 * [Construtor]
 *
 * Descrição: Executa o método construtor da
 * classe ElapsedTime.
 *
 * Parâmetros:
 *  - QObject *parent : Ponteiro para o QObject Pai *
 */
ElapsedTime::ElapsedTime(QObject *parent) : QObject(parent)
{
    //Define os atributos como 0
    clear();
}

/*
 * [Método]
 *
 * Descrição: Adiciona um minuto ao timer, em milissegundos.
 *
 * Parâmetros:
 *  - int msec : Quantidade de milissegundos passadas
 *  - int minuteSize : Quantos segundos se tem em 1 minuto, milissegundos
 */
void ElapsedTime::addMinute(int msec, int minuteSize)
{
    //A razão milissegundos passados/tamanho do minuto em milissegundos
    int min = msec/minuteSize;
    //Caso o número de minutos passados seja maior que 60, incrementa a hora
    //e subtrai 60 dos minutos, caso não, adicione m_min minutos
    if(m_min + min >= 60) {
        m_min = (m_min + min) - 60;
        addHour();
    } else {
        m_min += min;
    }
    //Emite um sinal dizendo que os minutos foram incrementados
    emit minuteHasBeenIncremented();
}


/*
 * [Método]
 *
 * Descrição: Retorna o atributo m_day
 *
 * Parâmetros:
 *  - void
 */
int ElapsedTime::day() const
{
    return m_day;
}


/*
 * [Método]
 *
 * Descrição: Retorna o atributo m_hour
 *
 * Parâmetros:
 *  - void
 */
int ElapsedTime::hour() const
{
    return m_hour;
}

/*
 * [Método]
 *
 * Descrição: Retorna o atributo m_min
 *
 * Parâmetros:
 *  - void
 */
int ElapsedTime::min() const
{
    return m_min;
}


/*
 * [Método]
 *
 * Descrição: Retorna todos os atributos a condição inicial
 *
 * Parâmetros:
 *  - void
 */
void ElapsedTime::clear()
{
    m_min = 0; m_hour = 0; m_day = 0;
}


/*
 * [Método]
 *
 * Descrição: Adiciona uma hora ao timer.
 *
 * Parâmetros:
 *  - void
 */
void ElapsedTime::addHour()
{
    //Verifica se a quantidade de horas supera 24, caso sim,
    //incrementa o dia, caso não, incrementa as horas do timer.
    if(m_hour + 1 >= 24) {
        m_hour = (m_hour + 1) - 24;
        addDay();
    } else {
        m_hour++;
    }
    //Emite um sinal dizendo que as horas foram incrementadas
    emit hourHasBeenIncremented();
}

/*
 * [Método]
 *
 * Descrição: Adiciona um dia ao timer.
 *
 * Parâmetros:
 *  - void
 */
void ElapsedTime::addDay()
{
    //Incrementa um dia ao timer
    m_day++;
    //Emite um sinal dizendo que já se passou 1 dia
    emit dayHasBeenIncremented();
}
