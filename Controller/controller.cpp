#include "controller.h"

/*
 * [Construtor]
 *
 * Descrição: Executa o método construtor da
 * classe Controller.
 *
 * Parâmetros:
 *  - QObject *parent : Ponteiro para o QObject Pai *
 */
Controller::Controller(QObject *parent) : QObject(parent)
{
    //Define que o número de Dias Passados é 0
    m_numDays = 0;
    //Define que o número de amostra coletadas é 0
    m_numSamples = 0;
    //Define que o Timer terá um intervalo de 3 minutos
    //antes de emitir o sinal timeout()
    m_timer = new Timer(180000/*3 min*/, this);

    //Conectando o sinal dayHasBeenIncremented()
    //com o slot storesStationData
    connect(m_timer->elapsedTime(), SIGNAL(dayHasBeenIncremented()),
            this, SLOT(storesStationData()));

    //Instância de um DataCollector
    m_dc = new DataCollector(this);

    //Conectando o sinal newDBState() ao slot
    //receivedNewRemoteDBState
    connect(m_dc, SIGNAL(newDBState(DBState*)),
            this, SLOT(receivedNewRemoteDBState(DBState*)));

    //Instância de um DataCollector
    m_dm = new DataManager(this);

    //Definição de um mapa de propriedades para instanciar o
    //estado inicial
    QVariantMap properties;
    properties.insert("date",QDateTime::currentDateTime());
    properties.insert("id", 0);
    properties.insert("wind_direction", 0);
    properties.insert("wind_velocity", 0);
    properties.insert("temperature", 0);
    properties.insert("humidity", 0);
    properties.insert("accumulated_rain", 0);
    properties.insert("pressure_atm", 0);
    properties.insert("luminosity", 0);

    //Instância do estado inicial
    m_meanState = new DBState(properties,this);

    //Instância de um DataProvider
    m_dp = new DataProvider(m_dm, m_meanState, this);

    //Conectando o sinal newDBState()
    //com o slot changeCurrentState
    connect(m_dc, SIGNAL(newDBState(DBState*)),
            m_dp, SLOT(changeCurrentState(DBState*)));

    //Conectando o sinal newMethodsCalculated()
    //com o slot addMethodsPoint
    connect(m_dm, SIGNAL(newMethodsCalculated(MethodGraphPoint*)),
            m_dp, SLOT(addMethodsPoint(MethodGraphPoint*)));
}

/*
 * [Método/Slot]
 *
 * Descrição: Salva os dados provenientes da
 * estação, enviando para o BD local e remoto.
 * Ao se passar 1 dia, ele salva uma média. Ao
 * se passar 3 dias, ele salva o valor estimado
 * de todos os métodos de Evapotranspiração.
 * Esse método é chamado a cada 1 dia.
 *
 * Parâmetros:
 *  - void
 */
void Controller::storesStationData(){
    //incrementa o número de dias passados
    m_numDays += 1;
    //Verifica se 66,667% da amostra diária já foi coletada
    if(m_numSamples >= 320){
        //Realiza o cálculo de uma média dos dados obtidos
        realizeMean();
        //salva a média definida anteriormente nos bancos
        //de dados local e remoto (firebase)
        m_dm->saveMeanDataMetStation(m_meanState);
    }
    //Checa se já se passaram 3 dias
    if(m_numDays == 3){
        //Salva o resultado dos métodos
        m_dm->saveMethodsResults();
        //Zera o número de dias contados
        m_numDays = 0;
    }
    //Limpa o tempo acumulado pelo timer
    m_timer->elapsedTime()->clear();
    //Zera o número de amostras acumuladas
    m_numSamples = 0;
    //Define o ID do próximo dado adicionado
    //pra 0
    m_dc->resetVersion();
}

/*
 * [Método/Slot]
 *
 * Descrição: Recebe o sinal de um novo estado
 * computado e verifica se o mesmo apresenta
 * algum erro. Caso não, ele adiciona o novo
 * estado ao banco de dados e soma ao estado
 * médio, o qual no fim irá realizar a média
 * dos dados somados. Ele também incrementa
 * em 1 o número de amostras.
 *
 * Parâmetros:
 *  - DBState *state : Novo Estado Computado
 */
void Controller::receivedNewRemoteDBState(DBState *state)
{
    //Verifica se o Estado apresenta algum erro
    if(state->get_error()) return;
    //Adiciona o novo estado ao banco de dados
    //local
    m_dm->addStateToDB(state);
    //Soma o novo estado ao estado médio
    addMeanState(state);
    //Incrementa em 1 o número de amostas
    m_numSamples += 1;
}


/*
 * [Método]
 *
 * Descrição: Soma um novo estado computado
 * ao estado médio, extraindo o mapa de pro-
 * priedades do novo estado e incrementa ao
 * mapa de propriedades do estado médio.
 *
 * Parâmetros:
 *  - DBState *newState : Novo Estado Computado
 */
void Controller::addMeanState(DBState *newState)
{
    //Criação de um mapa novo
    QVariantMap properties;
    //Inserção do timestamp atual ao estado médio
    properties.insert("date",QDateTime::currentDateTime());
    //Inserção de um ID arbitrário ao estado médio
    properties.insert("id", 0);
    //Soma das propriedades do estado médio com o
    //estado novo
    properties.insert("wind_direction", m_meanState->get_windDirection() + newState->get_windDirection());
    properties.insert("wind_velocity", m_meanState->get_windVelocity() + newState->get_windVelocity());
    properties.insert("temperature", m_meanState->get_temperature() + newState->get_temperature());
    properties.insert("humidity", m_meanState->get_humidity() + newState->get_humidity());
    properties.insert("accumulated_rain", m_meanState->get_accumulatedRain() + newState->get_accumulatedRain());
    properties.insert("pressure_atm", m_meanState->get_pressureATM() + newState->get_pressureATM());
    properties.insert("luminosity", m_meanState->get_luminosity() + newState->get_luminosity());

    //Atribuição do novo mapa de propriedades
    //ao estado médio
    m_meanState->setProperties(properties);
}

/*
 * [Método]
 *
 * Descrição: Pega os dados somados e atribuídos
 * ao estado médio e divide-os pela quantidade de
 * amostras coletadas, realizando assim, a média
 * aritmética dos dados coletados
 *
 * Parâmetros:
 *  - void
 */
void Controller::realizeMean()
{    
    //Criação de um mapa novo
    QVariantMap properties;

    //Realização da média aritmética de cada pro-
    //priedade do estado médio
    properties.insert("date",QDateTime::currentDateTime());
    properties.insert("id", 0);
    properties.insert("wind_direction", m_meanState->get_windDirection()/m_numSamples);
    properties.insert("wind_velocity", m_meanState->get_windVelocity()/m_numSamples);
    properties.insert("temperature", m_meanState->get_temperature()/m_numSamples);
    properties.insert("humidity", m_meanState->get_humidity()/m_numSamples);
    properties.insert("accumulated_rain", m_meanState->get_accumulatedRain()/m_numSamples);
    properties.insert("pressure_atm", m_meanState->get_pressureATM()/m_numSamples);
    properties.insert("luminosity", m_meanState->get_luminosity()/m_numSamples);

    //Atribuição do novo mapa de propriedades
    //ao estado médio
    m_meanState->setProperties(properties);
}

