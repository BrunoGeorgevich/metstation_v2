#ifndef TIMER_H
#define TIMER_H

#include <QObject>
#include <QTimer>
#include <QDebug>

#include "elapsedtime.h"

class Timer : public QObject
{
    Q_OBJECT
public:
    explicit Timer(int intervalMsec, QObject *parent = 0);
    ElapsedTime *elapsedTime() const;
private slots:
    void timeoutComplete();
private:
    int m_interval;
    QTimer *m_timer;
    ElapsedTime *m_elapsedTime;
};

#endif // TIMER_H
