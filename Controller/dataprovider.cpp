#include "dataprovider.h"

/*
 * [Construtor]
 *
 * Descrição: Executa o método construtor da
 * classe DataProvider.
 *
 * Parâmetros:
 *  - DataManager *dm : Ponteiro do DataManager
 *  - DBState *initialState : Ponteiro do estado inicial do Projeto
 *  - QObject *parent : Ponteiro para o QObject Pai *
 */
DataProvider::DataProvider(DataManager *dm, DBState *initialState, QObject *parent) : QObject(parent)
{
    //Atribuição do ponteiro do DataManager
    //a uma variável local
    m_dm = dm;

    //Inicia a lista de estados exibida na GUI
    initStateList(initialState);
    //Inicia a lista de métodos exibida na GUI
    initMethodsList();
}

/*
 * [Método/Slot]
 *
 * Descrição: Atualiza o valor do estado atual
 * do projeto. Além disso, ele checa se os de-
 * talhes dos gráficos estão corretos e atualiza
 * os itens dos mesmos.
 *
 * Parâmetros:
 *  - DBState *newState : Ponteiro do novo estado do Projeto
 */
void DataProvider::changeCurrentState(DBState *newState)
{
    //Atualiza o estado do projeto e emite um
    //sinal de atualização
    update_currentState(newState);

    //Verifica se o sistema já tem uma lista
    //de tamanho igual a 30
    if(m_states->size() == 30)
        //Se sim, remover o estado mais antigo
        m_states->remove(m_states->first());
    //Adicionar o novo estado a lista de estados
    m_states->append(newState);

    //Define uma variável auxiliar = aux
    bool aux = false;

    /*
     * Itera dentro do array dos objetos, que definem
     * os detalhes salvos dos gráficos, e verifica se
     * os gráficos precisam atualizar os seus dados
     */
    for (int i = 0; i < m_sDetails->size(); ++i) {
        //Define uma variável auziliar para acessar os
        //itens do array
        GraphDetails *details = m_sDetails->at(i);
        //Caso o aux seja verdadeiro, continua verdadeiro
        aux = aux || details->decrementIndex();
    }

    //Se aux é true, deve-se atualizar os dados dos gráficos
    if(aux)
        updateMaximumOfStatesGraph();
    //Se aux é false, deve-se verificar se os dados dos gráficos mudaram
    else
        verifyMaximumOfStatesGraphHaschanged(newState, m_states->size() - 1);
    //Emite um sinal de atualização para que o gráfico se atualize
    emit updateStatesChart();
}

/*
 * [Método/Slot]
 *
 * Descrição: Verifica se a lista de pontos de métodos
 * é menor ou igual a 30, se sim, remove o ponto mais
 * antigo, caso não, somente adiciona o ponto mais novo
 * a lista. Depois, verifica se os detalhes do gráfico
 * estão atualizados e englobando os dados visualizados.
 *
 * Parâmetros:
 *  - MethodGraphPoint *newPoint : Ponteiro do novo ponto
 *    dos métodos
 */
void DataProvider::addMethodsPoint(MethodGraphPoint *newPoint)
{
    //Verifica se a lista de métodos é menor ou igual a 30
    if(m_methods->size() == 30)
        //Se sim, remover o ponto mais antigo
        m_methods->remove(m_methods->first());

    //Adicionar o novo ponto a lista
    m_methods->append(newPoint);

    //Verificar se o detalhe definido do gráfico
    //já foi removido
    if(m_mDetails->decrementIndex())
        //Atualiza os detalhes do gráfico
        updateMaximumOfMethodGraph();
    else
        //Verifica se o máximo do Gráfico mudou
        verifyIfMaximumOfMethodGraphHasChanged(newPoint);

    //Emite um sinal de atualização dos dados do gráficos de métodos
    emit updateMethodsChart();
}


/*
 * [Método]
 *
 * Descrição: Instancia e inicia a lista de estados e o
 * estado atual. Também inicia a lista de propriedades
 * do gráfico de dados.
 *
 * Parâmetros:
 *  - DBState *initialState : Ponteiro do novo estado
 */
void DataProvider::initStateList(DBState *initialState)
{
    //Instancia a lista de estados
    m_states = new QQmlObjectListModel<DBState>(this);
    //Atualiza o estado atual
    update_currentState(initialState);

    //Instancia a lista de propriedades do gráfico de dados
    m_sDetails = new QQmlObjectListModel<GraphDetails>(this);

    //Instancia cada propriedade
    for(int i = 0; i < 5; i++)
        m_sDetails->append(new GraphDetails());

    //Recupera do banco de dados local os últimos 30 estados
    foreach (DBState *state, m_dm->retrieveLastStatePoints()) {
        m_states->append(state);
    }

    //Se a lista não for vazia, ele calcula as propriedades
    //do gráfico
    if(m_states->size())
        updateMaximumOfStatesGraph();
}

/*
 * [Método]
 *
 * Descrição: Instancia e inicia a lista dos métodos, também calcula
 * as propriedades do gráfico.
 *
 * Parâmetros:
 *  - void
 */
void DataProvider::initMethodsList()
{
    //Instancia a lista de Métodos
    m_methods = new QQmlObjectListModel<MethodGraphPoint>(this);
    //Instancia as propridades do gráfico
    m_mDetails = new GraphDetails();

    //Recupera os últimos 30 pontos de métodos
    foreach (MethodGraphPoint *mgPoint, m_dm->retrieveLastMethodsPoints()) {
        m_methods->append(mgPoint);
    }

    //Se a lista não estiver vazia, calcula as propriedades do gráfico
    if(m_methods->size())
        updateMaximumOfMethodGraph();
}

/*
 * [Método]
 *
 * Descrição: Calcula as propriedades do gráfico
 * dos métodos, como valor máximo da amostragem e
 * o índice desse valor máximo. Visando diminuir o
 * número de atualizações da interface gráfica.
 *
 * Parâmetros:
 *  - MethodGraphPoint *newPoint : Ponteiro do ponto dos
 * métodos
 */
float DataProvider::verifyIfMaximumOfMethodGraphHasChanged(MethodGraphPoint *mgPoint)
{
    //Pega o maior valor já calculado
    float aux = m_mDetails->get_majorValue();

    //Verifica se esse método tem o valor maior do que o previsto
    m_mDetails->set_majorValue(
            (mgPoint->get_camargo() >= m_mDetails->get_majorValue())
            ? mgPoint->get_camargo()
            : m_mDetails->get_majorValue()
              );

    //Verifica se esse método tem o valor maior do que o previsto
    m_mDetails->set_majorValue(
            (mgPoint->get_penmanmonteith() >= m_mDetails->get_majorValue())
            ? mgPoint->get_penmanmonteith()
            : m_mDetails->get_majorValue()
              );

    //Verifica se esse método tem o valor maior do que o previsto
    m_mDetails->set_majorValue(
            (mgPoint->get_thornwaite() >= m_mDetails->get_majorValue())
            ? mgPoint->get_thornwaite()
            : m_mDetails->get_majorValue()
              );

    return m_mDetails->get_majorValue() == aux ? -1 : m_mDetails->get_majorValue();
}

/*
 * [Método]
 *
 * Descrição: Calcula a lista de propriedades do gráfico
 * dos estados, como os valores máximos das amostragens e
 * os índices desses valores. Visando diminuir o
 * número de atualizações da interface gráfica.
 *
 * Parâmetros:
 *  - DBState *state : Ponteiro para o estado de teste
 *  - int index : Representa o índice desse estado teste
 */
bool DataProvider::verifyMaximumOfStatesGraphHaschanged(DBState *state, int index)
{
    //Passa os valores das propriedades previamente calculadas
    //para um mapa de valores
    QMap<QByteArray, GraphDetails*> auxMap = toMap();

    //Define uma variável auxiliar: check
    bool check = false;

    //Itera entre todos os atributos do estado, buscando algum
    //que supere os dados já calculados
    foreach (QByteArray str, auxMap.keys()) {
        if(state->getProperty(str) >= auxMap.value(str)->get_majorValue()){
            auxMap[str]->set_majorIndex(index);
            auxMap[str]->set_majorValue(state->getProperty(str));
            check = true;
        }
    }
    //Caso ele tenha achado algum dado que supere o valor máximo
    //do que já foi, previamente, calculado, ele insere os novos
    //valores de volta na lista de propriedades do gráfico de estados
    if(check)
        fromMap(auxMap);

    return check;
}


/*
 * [Método]
 *
 * Descrição: Atualiza o valor máximo e índice do
 * objeto que guarda as propriedades do gráfico dos
 * métodos.
 *
 * Parâmetros:
 *  - void
 */
void DataProvider::updateMaximumOfMethodGraph()
{
    //Define os atributos como 0, para buscar novos
    //máximos
    m_mDetails->set_majorIndex(0);
    m_mDetails->set_majorValue(0);

    //Caso tenha altera, define essa variável como true
    bool hasChanged = false;
    //Itera entre os itens testando que é o maior
    for (int i = 0; i < m_methods->size(); i++) {
        float majorMethodValue = verifyIfMaximumOfMethodGraphHasChanged(m_methods->at(i));
        if(majorMethodValue != -1) {
            m_mDetails->set_majorIndex(i);
            m_mDetails->set_majorValue(majorMethodValue);
            hasChanged = true;
        }
    }

    //Insere um log no banco de dados
    m_dm->addLogToDB(" METHODS GRAPH MAXIMUM HAS CHANGED : " + hasChanged);

    //Verifica se algum dado foi alterado
    //Se sim, emite um sinal de atualização para o gráfico
    if(hasChanged)
        emit methodsChartDetailsHasChanged(m_mDetails);
}

/*
 * [Método]
 *
 * Descrição: Atualiza os valores máximos e os índices da
 * lista que guarda as propriedades dos gráficos dos
 * estados.
 *
 * Parâmetros:
 *  - void
 */
void DataProvider::updateMaximumOfStatesGraph()
{
    //Zera a lista de propriedades do gráfico
    m_sDetails->clear();
    //Reinsere dados zerados na lista
    for(int i = 0; i < 5; i++)
        m_sDetails->append(new GraphDetails());

    //Cria uma variável teste para verificar se houve
    //alteração nas propriedades dos gráficos
    bool hasChanged = false;
    for (int i = 0; i < m_states->size(); i++) {
        hasChanged = verifyMaximumOfStatesGraphHaschanged(m_states->at(i), i);
    }

    //Adiciona um log no banco de dados local
    m_dm->addLogToDB(" STATES GRAPH MAXIMUM HAS CHANGED : " + hasChanged);

    //Verifica se houve alteração, se sim
    //emite um sinal de atualização ao gráfico de propriedades
    if(hasChanged)
        emit statesChartDetailsHasChanged(m_sDetails);
}

/*
 * [Método]
 *
 * Descrição: Retorna uma cópia da lista de propriedades em
 * formato de mapa.
 *
 * Parâmetros:
 *  - void
 */
QMap<QByteArray, GraphDetails *> DataProvider::toMap()
{
    //Cria um mapa vazio
    QMap<QByteArray, GraphDetails*> auxMap;
    auxMap.insert("temperature", new GraphDetails());
    auxMap.insert("luminosity", new GraphDetails());
    auxMap.insert("accumulated_rain", new GraphDetails());
    auxMap.insert("humidity", new GraphDetails());
    auxMap.insert("wind_velocity", new GraphDetails());

    //Insere os dados da lista no mapa
    auxMap["temperature"] = m_sDetails->at(0);
    auxMap["luminosity"] = m_sDetails->at(1);
    auxMap["accumulated_rain"] = m_sDetails->at(2);
    auxMap["humidity"] = m_sDetails->at(3);
    auxMap["wind_velocity"] = m_sDetails->at(4);

    return auxMap;
}

/*
 * [Método]
 *
 * Descrição: Atualiza os dados, provenientes de um mapa, da
 * lista de propriedades dos gráficos.
 *
 * Parâmetros:
 *  - QMap<QByteArray, GraphDetails *> map : Mapa com os novos dados
 */
void DataProvider::fromMap(QMap<QByteArray, GraphDetails *> map)
{
    //Atualiza os dados da lista de propriedades
    m_sDetails->at(0)->set_majorIndex(map["temperature"]->get_majorIndex());
    m_sDetails->at(0)->set_majorValue(map["temperature"]->get_majorValue());
    m_sDetails->at(1)->set_majorIndex(map["luminosity"]->get_majorIndex());
    m_sDetails->at(1)->set_majorValue(map["luminosity"]->get_majorValue());
    m_sDetails->at(2)->set_majorIndex(map["accumulated_rain"]->get_majorIndex());
    m_sDetails->at(2)->set_majorValue(map["accumulated_rain"]->get_majorValue());
    m_sDetails->at(3)->set_majorIndex(map["humidity"]->get_majorIndex());
    m_sDetails->at(3)->set_majorValue(map["humidity"]->get_majorValue());
    m_sDetails->at(4)->set_majorIndex(map["wind_velocity"]->get_majorIndex());
    m_sDetails->at(4)->set_majorValue(map["wind_velocity"]->get_majorValue());
}
