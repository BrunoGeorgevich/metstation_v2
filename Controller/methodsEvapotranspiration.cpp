#include "methodsEvapotranspiration.h"

/*
 * [Singleton]
 *
 * Descrição: É a definição do atributo m_intance.
 *
 * Parâmetros:
 *  - void
 */
MethodsEvapotranspiration *MethodsEvapotranspiration::m_instance = 0;

/*
 * [Construtor]
 *
 * Descrição: Executa o método construtor da
 * classe MethodsEvapotranspiration.
 *
 * Parâmetros:
 *  - QObject *parent : Ponteiro para o QObject Pai *
 */
MethodsEvapotranspiration::MethodsEvapotranspiration(QObject *parent) : QObject(parent)
{
    //Inicia a lista de Fotoperíodo de cada mês do ano
    initPhotoPeriod();
    //Inicia a lista de Radiação Solar de cada mês do ano
    initSolarRadiation();
}

/*
 * [Método]
 *
 * Descrição: Inicia a lista de Radiação Solar de cada mês do ano.
 *
 * Parâmetros:
 *  - void
 */
void MethodsEvapotranspiration::initSolarRadiation()
{
    m_solarRadiation.insert("jan", 15.7);
    m_solarRadiation.insert("fev", 15.6);
    m_solarRadiation.insert("mar", 15.0);
    m_solarRadiation.insert("abr", 13.9);
    m_solarRadiation.insert("mai", 12.6);
    m_solarRadiation.insert("jun", 11.9);
    m_solarRadiation.insert("jul", 11.7);
    m_solarRadiation.insert("ago", 13.1);
    m_solarRadiation.insert("set", 14.5);
    m_solarRadiation.insert("out", 15.3);
    m_solarRadiation.insert("nov", 15.5);
    m_solarRadiation.insert("dez", 15.5);
}


/*
 * [Método]
 *
 * Descrição: Inicia a lista de Fotoperíodo de cada mês do ano.
 *
 * Parâmetros:
 *  - void
 */
void MethodsEvapotranspiration::initPhotoPeriod()
{
    m_photoPeriod.insert("jan", 12.6);
    m_photoPeriod.insert("fev", 12.4);
    m_photoPeriod.insert("mar", 12.2);
    m_photoPeriod.insert("abr", 11.9);
    m_photoPeriod.insert("mai", 11.7);
    m_photoPeriod.insert("jun", 11.6);
    m_photoPeriod.insert("jul", 11.7);
    m_photoPeriod.insert("ago", 11.8);
    m_photoPeriod.insert("set", 12.1);
    m_photoPeriod.insert("out", 12.3);
    m_photoPeriod.insert("nov", 12.5);
    m_photoPeriod.insert("dez", 12.7);
}

/*
 * [Método]
 *
 * Descrição: Retorna um Singleton da classe MethodsEvapotranspiration.
 *
 * Parâmetros:
 *  - void
 */
MethodsEvapotranspiration *MethodsEvapotranspiration::getInstance()
{
    //Verifica se o objeto já foi instanciado, se sim, apenas o retorna,
    //se não, o instância antes
    if(!m_instance) m_instance = new MethodsEvapotranspiration();
    return m_instance;
}

/*
 * [Método]
 *
 * Descrição : Calcula o quanto de água foi perdida na evapotranspiração
 * utilizando do método desenvolvido por Charles Warren Thornthwaite.
 *
 * Parâmetros:
 *  - float averageTemp : Temperatura média
 *  - float yearTemp : Temperatura Anual média
 */
float MethodsEvapotranspiration::getThornthwaite(float avarageTemp, float yearTemp){
    float heatIndex=0, a=0, etp=0, correction=0;

    heatIndex = 12*pow(0.2*yearTemp,1.514);
    a = 0.49239+(1.7912*pow(10,-2))*heatIndex-
            (7.71*pow(10,-5))*pow(heatIndex,2) + (6.75*pow(10,-7))*pow(heatIndex,3);

    if(avarageTemp<26.5){
        etp = 16*pow(10*(avarageTemp/heatIndex),a);
    } else {
        etp = (-415.85)+(32.24*avarageTemp)-(0.43*pow(avarageTemp,2));
    }
    correction = (m_photoPeriod.value(QDate::currentDate().toString("MMM")).toFloat()/(12*30));

    return (etp*correction)*ThreeDays;
}

/*
 * [Método]
 *
 * Descrição : Calcula o quanto de água foi perdida na evapotranspiração
 * utilizando do método desenvolvido por Camargo.
 *
 * Parâmetros:
 *  - float averageTemp : Temperatura média
 */
float MethodsEvapotranspiration::getCamargo(float avarageTemp){
    float etp=0;

    etp = 0.01*m_solarRadiation.value(QDate::currentDate().toString("MMM")).toFloat()*
            avarageTemp*ThreeDays;

    return etp;
}

/*
 * [Método]
 *
 * Descrição : Calcula o quanto de água foi perdida na evapotranspiração
 * utilizando do método desenvolvido por Howard Penman e John Monteith.
 *
 * Parâmetros:
 *  - float averageTemp : Temperatura média
 *  - float humidity : Humidade do ambiente
 *  - float atmPressure : Pressão atmosférica
 *  - float windSpeed : Velocidade do vento
 */
float MethodsEvapotranspiration::getPenmanMonteith(float avarageTemp, float humidity, float atmPressure, float windSpeed){
    float etp=0, saturationPressure=0, parcialPressure=0,
            pressureCurveDeclivity=0, netIrradiance=0, psychrometricConst=0;

    saturationPressure = 0.611*pow(10,((7.5*avarageTemp)/(237.3+avarageTemp)));
    pressureCurveDeclivity = (4098*saturationPressure)/pow(237.3+avarageTemp,2);
    parcialPressure = (humidity*saturationPressure)/100;
    netIrradiance = 0.653*m_solarRadiation.value(QDate::currentDate().toString("MMM")).toFloat();
    psychrometricConst = 0.665*0.001*atmPressure;
    etp = ((0.408*pressureCurveDeclivity*netIrradiance)+psychrometricConst*(900/(avarageTemp+273))*windSpeed*
           (saturationPressure-parcialPressure)) /
            (pressureCurveDeclivity + psychrometricConst*(1 + 0.34*windSpeed));

    return etp*ThreeDays;
}

/*
 * [Método]
 *
 * Descrição : Retorna a quantidade de Radiação solar de
 * um determinado mês.
 *
 * Parâmetros:
 *  - QString month : Mês determminado
 */
float MethodsEvapotranspiration::getSolarRadiantion(QString month)
{
    return m_solarRadiation.value(month).toFloat();
}

/*
 * [Método]
 *
 * Descrição : Retorna a quantidade de Fotoperíodo de
 * um determinado mês.
 *
 * Parâmetros:
 *  - QString month : Mês determminado
 */
float MethodsEvapotranspiration::getPhotoPeriod(QString month)
{
    return m_photoPeriod.value(month).toFloat();
}







