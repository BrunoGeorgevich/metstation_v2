#include "methodgraphpoint.h"

/*
 * [Construtor]
 *
 * Descrição: Executa o método construtor da
 * classe MethodGraphPoint.
 *
 * Parâmetros:
 *  - float t : Representa o valor do método de Thronwaite
 *  - float p : Representa o valor do método de Penman-Monteith
 *  - float c : Representa o valor do método de Camargo
 *  - QObject *parent : Ponteiro para o QObject Pai *
 */
MethodGraphPoint::MethodGraphPoint(float t, float p, float c, QObject *parent) : QObject(parent)
{
    set_thornwaite(t); set_penmanmonteith(p); set_camargo(c);
}
