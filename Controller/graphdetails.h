#ifndef METHODGRAPHDETAILS_H
#define METHODGRAPHDETAILS_H

#include "../Essencials/qqmlhelpers.h"

class GraphDetails : public QObject {
    Q_OBJECT
    QML_WRITABLE_PROPERTY(int, majorIndex)
    QML_WRITABLE_PROPERTY(float, majorValue)
public:
    GraphDetails(): m_majorIndex(0), m_majorValue(0) {}
    bool decrementIndex() {
        m_majorIndex -= 1;
        if(m_majorIndex < 0)
            return true;
        return false;
    }
};

#endif // METHODGRAPHDETAILS_H
