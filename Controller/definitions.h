#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#define WINDDIRECTION 1
#define WINDVELOCITY 2
#define TEMPERATURE 3
#define HUMIDITY 4
#define ACCUMULATEDRAIN 5
#define PRESSUREATM 6
#define LUMINOSITY 7
#define BATTERY 8

#define THREEDAYS 1440 //1440 amostras que foram pegas a cada 3 min. durante 3 dias
#define ONEDAY 480 //480 amostras que foram pegas a cada 3 min. durante 1 dia

#endif // DEFINITIONS_H
