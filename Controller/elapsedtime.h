#ifndef ELAPSEDTIME_H
#define ELAPSEDTIME_H

#include <QObject>
#include <QDebug>

class ElapsedTime : public QObject
{
    Q_OBJECT
public:
    explicit ElapsedTime(QObject *parent = 0);
    void addMinute(int msec, int minuteSize);
    int day() const;
    int hour() const;
    int min() const;
    void clear();
signals:
    void minuteHasBeenIncremented();
    void dayHasBeenIncremented();
    void hourHasBeenIncremented();
private:
    int m_day;
    int m_hour;
    int m_min;
    void addHour();
    void addDay();
};

#endif // ELAPSEDTIME_H
